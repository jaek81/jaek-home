var options = {
		'legend':{
			names: [
				'Perceivable',
				'Information Loss',
				'Understandable',
				'Enough Time',
				'Epilepsy Prevent',
				'Operable',
				'Navigation',
				'Error Prevent'
			],
			hrefs: [
				'http://nuli.navercorp.com//sharing/a11y#k1',
				'http://nuli.navercorp.com//sharing/a11y#k2',
				'http://nuli.navercorp.com//sharing/a11y#k3',
				'http://nuli.navercorp.com//sharing/a11y#k4',
				'http://nuli.navercorp.com//sharing/a11y#k5',
				'http://nuli.navercorp.com//sharing/a11y#k6',
				'http://nuli.navercorp.com//sharing/a11y#k7',
				'http://nuli.navercorp.com//sharing/a11y#k8'
			]
		},
		'dataset': {
			title: 'Web accessibility status',
			values: [[34,53,67,23,78,45,69,98]],
			bgColor: '#f9f9f9',
			fgColor: '#30a1ce',
		},
		'chartDiv': 'Nwagon',
		'chartType': 'radar',
		'chartSize': { width: 500, height: 300 }
	};
	Nwagon.chart(options);